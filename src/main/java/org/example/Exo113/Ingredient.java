package org.example.Exo113;

public class Ingredient {

    String nom_aliment,etat;
    int quantite;

    String unite;

    Ingredient(String n, String e, int q, String unite){
        this.nom_aliment=n;
        this.etat=e;
        this.quantite=q;
        this.unite=unite;

    }
    //etat ingredient: cuit, entier, cru, découpé ou combinaison
    //unite de poids: gramme, kg etc... de volume:l,ml,cl ou cardinalite
}
